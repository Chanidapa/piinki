***Setting***
Library     SeleniumLibrary

***Variables***
${CarIn}     xpath=(//div[@class='panel-main-product-text desktop'])
${Cartype}     xpath=(//div[@class='lable-text pl-7'])[1]
${popup}     xpath=(//div[@class='v-card__text']) 
${CartypeButton1}     xpath=(//div[@class='modal__cartype__button button__togglegroup--modalcarInfo--container'])[1]
${Toyota}    xpath=(//div[@class='modalcarbrand__button button__togglegroup--modalcarInfo--container'])[1]
${Camry}    xpath=(//div[@class='modal__carmodel__content'])[32]
${Version}    xpath=(//div[@class='modal__carsubmodel__content'])[12]
${Year}    xpath=(//div[@class='modal__caryear__content'])[23]
${prodtype}    xpath=(//div[@class='modal__producttype__content'])[1]
${submit}    xpath=(//button[@id='producttype__submit--custom'])
${quotecard}    xpath=(//div[@class='col col-10'])[2]



***keyword***
Open Browser
    SeleniumLibrary.Open Browser    https://insurance.tescolotusmoney.com/    browser=chrome
    Seleniumlibrary.Maximize Browser Window
    
Wait to see car insurance
       Seleniumlibrary.Wait Until Element Is Visible     ${CarIn}    timeout=30 sec
     
Click 
       Seleniumlibrary.Click Element    ${Cartype}
       SeleniumLibrary.Wait Until Element Is Visible    ${CartypeButton1}    timeout=30 sec

Click Car Type 1
       Seleniumlibrary.Click Element    ${CartypeButton1}
       SeleniumLibrary.Wait Until Element Is Visible    ${Toyota}    timeout=30 sec
      

Select Toyota
       Seleniumlibrary.Click Element    ${Toyota}
       SeleniumLibrary.Wait Until Element Is Visible    ${Camry}    timeout=30 sec
      

Select Camry
       Seleniumlibrary.Click Element    ${Camry}
       SeleniumLibrary.Wait Until Element Is Visible    ${Version}    timeout=30 sec
   

Select Version
       Seleniumlibrary.Click Element    ${Version}
       SeleniumLibrary.Wait Until Element Is Visible    ${Year}    timeout=30 sec
      

Select Year
       Seleniumlibrary.Click Element    ${Year}
       SeleniumLibrary.Wait Until Element Is Visible    ${prodtype}    timeout=30 sec
    

Select Product Type
       Seleniumlibrary.Click Element    ${prodtype}
       SeleniumLibrary.Wait Until Element Is Visible    ${submit}    timeout=30 sec
   

Click Submit
       Seleniumlibrary.Click Element    ${submit}
       SeleniumLibrary.Wait Until Element Is Visible    ${quotecard}    timeout=30 sec


***Test Case***

Open Browser
     Open Browser
     Wait to see car insurance

Click Car In
     Click

Click Car Type 
     Click Car Type 1

Select Brand
     Select Toyota

Select Camry 
     Select Camry

Select Version
     Select Version

Select Year
     Select Year

Select Product Type
     Select Product Type

Click Submit
     Click Submit

